
(* Todos los elementos de una lista determinada cumplen cierta propiedad *)
Inductive all_in {X:Type}: (X->Prop) -> list X -> Prop :=
|all_in_trivial 
(P :X->Prop)
(l: list X)
(a:X)
(H1: P a):
all_in P (a::nil)
(*|all_in_trivial (P :X->Prop) : all_in P []*)
|all_in_each 
(P :X->Prop)
(l: list X)
(a:X)
(H1: P a)
(H2: all_in P l):
all_in P (a::l).