Require Import List.
Require Import Peano.
Require Import Coq.Program.Wf.

Notation "[ ]" := nil.

Notation "[ x , .. , y ]" := (cons x .. (cons y nil) ..).
(* [1] -> [2] -> [3] -> [4] -> [5] -> [6]
   [1] <- [2] -> [3, 4, 5, 6]
   [1, 2] <- [3] -> [4, 5, 6] *)
   
(* Se definen las estructuras basicas para un zipper de arboles binarios
igual se definen propiedades de igualdad de las estructuras*)
Section Zipper_estructura.

    Inductive tree (X:Type) : Type :=
    | nilTree : tree X
    | branch  : tree X -> X -> tree X -> tree X.

    Definition leaf {X:Type} (a:X): tree X := 
    branch X (nilTree X) a (nilTree X). 

    Inductive context (X:Type) : Type :=
    | top: context X
    | rightContext: X -> tree X -> context X -> context X
    | leftContext:  tree X -> X -> context X -> context X.

    Inductive zipper (X:Type) :Type := 
    | hole:  tree X -> context X -> zipper X.


End Zipper_estructura.

Section Funciones.

    (* Se mueve al hijo izquierdo desde el padre *)
    Definition go_left {X:Type}(z :zipper X): zipper X:=
     match z with 
       |hole nilTree _ => z
       |hole (branch l t r) c => hole X l (rightContext X t r c) 
     end.

    (* Se mueve al hijo derecho desde el padre *)
    Definition go_right {X:Type}(z :zipper X): zipper X:=
     match z with 
       |hole nilTree _ => z
       |hole (branch l t r) c => hole X r (leftContext X l t c)  
     end.

    (* Se mueve de cualquier hijo al padre*)
    Definition go_up {X:Type}(z :zipper X): zipper X:=
     match z with 
       |hole  _  top => z
       |hole  l (rightContext t r c)=>  hole X (branch X l t r) c
       |hole  r (leftContext l t c)=>  hole X (branch X l t r) c
     end.

    (* Se meuve la otro hijo, si esta en el derecho al izquierdo y viceversa*)
    Definition go_sibling {X:Type}(z :zipper X): zipper X:=
     match z with 
       |hole  _  top => z
       |hole  l (rightContext t r c)=>  hole X r (leftContext X l t c)
       |hole  r (leftContext l t c)=>  hole X l (rightContext X t r c)
     end.

    (* Actualiza un elemento *)
    Definition update {X:Type}(n :X)(z :zipper X) : zipper X :=
     match z with
       |hole nilTree _ => z
       |hole (branch l t r) c => hole X (branch X l n r) c 
    end.

    (* Regresa el elemento enfocado *)
    Definition get_focus {X:Type}(t :zipper X) : option X := 
    match t with
       |hole nilTree _ => None 
       |hole (branch _ t _) _ => Some t
    end. 

    (* Transforma un arbol en zipper *)
    Definition tree_to_zipper {X:Type}(t : tree X) : zipper X := hole X t (top X).

    Fixpoint tree_from_context {X:Type} (t : tree X)(c : context X) : tree X :=
    match c with
      |top => t
      |rightContext f r c1 => tree_from_context (branch X t f r) c1
      |leftContext l f c1 => tree_from_context (branch X l f t) c1
    end.

    (* Regresa un arbol a partir de un zipper*)
    Definition zipper_to_tree {X:Type}(z : zipper X) : tree X :=
    match z with
     |hole t c => tree_from_context t c
    end.

    (* Navega hasta la punta del zipper *)
    Definition move_zipper_top {X:Type}(z : zipper X) : zipper X :=
    match z with
     |hole t c => hole X (tree_from_context t c) (top X)
    end.

    Fixpoint context_depth {X:Type}(c :context X) : nat :=
    match c with 
       |top => 0
       |rightContext _ _ c1 => S (context_depth(c1))
       |leftContext _ _  c2 => S (context_depth(c2))
    end.

    (* Regresa a la que se encuentra el zipper *)
    Fixpoint zipper_depth {X:Type} (z : zipper X) : nat :=
    match z with
     |hole _ c => context_depth(c)
    end.

    (*Aplica una serie de funciones al zipper *)
    Fixpoint apply_functions {X:Type} (l :list (zipper X -> zipper X))(z :zipper X) : zipper X :=
    match l with
    | nil => z
    | a::l0 =>  apply_functions l0 (a z)
    end.

    (* Se definen las funciones que operan sobre zippers y no alteran la estructura del arbol y del contexto del zipper *)
    Definition unaltering_function {X:Type} (f:zipper X -> zipper X) :Prop := 
    forall z:zipper X, move_zipper_top(f z) = move_zipper_top z.

End Funciones.