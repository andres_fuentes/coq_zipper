Load Defs_Zipper.
Load Defs_Extras.

Section Propiedades_Zipper.

 
    Proposition le_after_up: forall X:Type,forall z:zipper X, zipper_depth (go_up(z)) <= zipper_depth z.
    intros.
    induction z.
    induction c.
    + unfold go_up.
      simpl.
      constructor.
    + unfold go_up.
      simpl.
      constructor.
      constructor.
    + unfold go_up.
      simpl.
      constructor.
      constructor.
    Qed.

    Proposition inverse_go_left: forall X:Type, forall c:context X,forall t:tree X,
    (~ ((nilTree X) = t)) ->  (go_up(go_left((hole X t c)))) =  (hole X t c).
    Proof.
    intros.
    induction c.
    + unfold go_left.
      destruct t.
      - constructor.
      - constructor.
    + unfold go_left.
      unfold go_up.
      destruct t.
      - contradict H.
        constructor.
      - constructor.
    + unfold go_left.
      unfold go_up.
      destruct t.
      - contradict H.
        constructor.
      - constructor.
    Qed.

    Proposition inverse_go_right: forall X:Type, forall c:context X,forall t:tree X, 
    (~ ((nilTree X) = t)) ->  (go_up(go_left((hole X t c)))) = (hole X t c).
    Proof.
    intros.
    induction c.
    + unfold go_right.
      destruct t.
      - constructor.
      - constructor.
    + unfold go_right.
      unfold go_up.
      destruct t.
      - contradict H.
        constructor.
      - constructor.
    + unfold go_right.
      unfold go_up.
      destruct t.
      - contradict H.
        constructor.
      - constructor.
    Qed.

    Proposition inverse_go_sibling: forall X:Type, forall c:context X,forall t:tree X, 
    (~ ((nilTree X) = t)) ->  (go_sibling(go_sibling((hole X t c)))) = (hole X t c).
    Proof.
    intros.
    induction c.
    + unfold go_sibling.
      reflexivity.
    + unfold go_sibling.
      reflexivity.
    + unfold go_sibling.
      reflexivity.
    Qed.


    Proposition is_unaltering: forall X:Type,forall f:zipper X -> zipper X,
    (forall z: zipper X,  move_zipper_top z =  move_zipper_top (f z)) <-> unaltering_function f.
    Proof.
    intros.
    split.
    + unfold unaltering_function.
      intros.
      specialize H with z.
      rewrite H.
      reflexivity.
    + intros.
      unfold unaltering_function in H.
      specialize H with z.
      rewrite H.
      reflexivity.
     Qed.

    Proposition go_left_is_unaltering: forall X:Type, unaltering_function  (@go_left X).
    intros.
    unfold unaltering_function.
    intros.
    destruct z.
    simpl.
    destruct t.
    + simpl.
      reflexivity.
    + simpl.
      reflexivity.
    Qed.

    Proposition go_right_is_unaltering: forall X:Type, unaltering_function  (@go_right X).
    intros.
    unfold unaltering_function.
    intros.
    destruct z.
    simpl.
    destruct t.
    + reflexivity.
    + simpl.
      reflexivity.
    Qed.
    

    Proposition go_up_is_unaltering: forall X:Type, unaltering_function  (@go_up X).
    intros.
    unfold unaltering_function.
    intros.
    destruct z.
    simpl.
    destruct c.
    + reflexivity.
    + simpl.
      reflexivity.
    + simpl.
      reflexivity.
    Qed.

    Proposition go_sibling_is_unaltering: forall X:Type, unaltering_function  (@go_sibling X).
    intros.
    unfold unaltering_function.
    intros.
    destruct z.
    simpl.
    destruct c.
    + reflexivity.
    + simpl.
      reflexivity.
    + simpl.
      reflexivity.
    Qed.


    Proposition chaining_unaltering_functions: forall X:Type, forall operations:list (zipper X -> zipper X), forall a:zipper X -> zipper X, 
   (unaltering_function a /\ unaltering_function (apply_functions  operations)) -> unaltering_function (apply_functions (a :: operations)).
    Proof.
    intros.
    unfold unaltering_function.
    intros.
    destruct H.
    simpl.
    unfold unaltering_function in  H0.
    specialize H0 with (a z).
    unfold unaltering_function in  H.
    specialize H with z.
    rewrite H0.
    rewrite H.
    reflexivity.
    Qed.

    Proposition composition_of_unaltering_is_unalterng: forall X:Type,forall operations:list (zipper X -> zipper X),
    (all_in unaltering_function operations) -> unaltering_function (apply_functions operations).
    Proof. 
    intros.
    induction operations.
    - constructor.
    - inversion H.
      + apply chaining_unaltering_functions.
        split.
        * assumption.
        * constructor.
      + apply chaining_unaltering_functions.
        split.
        * assumption.
        * apply IHoperations.
          assumption.
    Qed.

    Proposition apply_unaltering_functions: forall X:Type,forall operations:list (zipper X -> zipper X),
    (all_in unaltering_function operations) -> (forall t:tree X,tree_to_zipper t = move_zipper_top(apply_functions operations (tree_to_zipper t))).
    Proof.
    intros.
    assert(H0:  move_zipper_top (tree_to_zipper t) = (tree_to_zipper t)).
    - unfold move_zipper_top.
      destruct t.
      + simpl.
        reflexivity.
      + simpl.
        reflexivity.
    - rewrite <- H0.
      apply is_unaltering.
      apply  composition_of_unaltering_is_unalterng.
      assumption.
    Qed.

End Propiedades_Zipper.