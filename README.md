## **Proyecto**

## Informacion General

**No Cuenta:**  520014316

**Alumno:**  Andres Fuentes Hernandez

**Proyecto:**  git clone https://andres_fuentes@bitbucket.org/andres_fuentes/coq_zipper.git


## Ejecucion


1.- En el archivo Defs_Zipper se encuentran las definiciones sobre de arboles y de la estructura zipper.

2.- En el archivo Defs_Extras se encuentra una sola definicion sobre listas

3.- En el archivo Props_Zipper contiene las propiedades que se demostraron para el proyecto

4.- El archivo Documento_Zipper contiene el escrito del proyecto

